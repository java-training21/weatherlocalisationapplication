package com.example.WeatherLocalisation.Controller;


        import com.example.WeatherLocalisation.Entity.Localisation;
        import com.example.WeatherLocalisation.Repository.LocalisationRepository;
        import com.example.WeatherLocalisation.Service.LocalisationService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.*;

        import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class LocalisationController {

    @Autowired
    private LocalisationRepository localisationRepository;
    @Autowired
    LocalisationService localisationService;


    @PostMapping("/localisation")
    public void add(@RequestBody Localisation localisation) {
        localisationService.saveLocalisation(localisation);
    }
//    public Localisation createNewCurrency(@RequestBody Localisation currency) {  //@Valaid przed @ReequestBody
//        return localisationRepository.save(currency);
//    }

//    @GetMapping("/currencies/{id}")
//    public ResponseEntity<Currency> getEmployeeById(@PathVariable(value = "id") Long currencyId)
//            throws ResourceNotFoundException {
//        Currency currency = currencyRepository.findById(currencyId)
//                .orElseThrow(() -> new ResourceNotFoundException("Currency not found for id : " + currencyId));
//        return ResponseEntity.ok().body(currency);
//    }


    @GetMapping("/localisation")
    public List<Localisation> getAllLocalisation() {
        return localisationRepository.findAll();
    }
//nie dziala z CrudRepository











}
