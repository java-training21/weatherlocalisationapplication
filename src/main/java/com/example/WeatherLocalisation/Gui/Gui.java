package com.example.WeatherLocalisation.Gui;

//https://www.youtube.com/watch?v=AYuLKHK4PIk

public class Gui {
    String cityName;
//    int latValue;
//    int lonValue;

//    public Gui(String cityName, int latValue, int lonValue) {
//        this.cityName = cityName;
//        this.latValue = latValue;
//        this.lonValue = lonValue;
//    }

    public Gui(String cityName) {
        this.cityName = cityName;
    }

    public Gui() {

    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

//    public int getLatValue() {
//        return latValue;
//    }
//
//    public void setLatValue(int latValue) {
//        this.latValue = latValue;
//    }
//
//    public int getLonValue() {
//        return lonValue;
//    }
//
//    public void setLonValue(int lonValue) {
//        this.lonValue = lonValue;
//    }


    @Override
    public String toString() {
        return cityName;
    }
}
