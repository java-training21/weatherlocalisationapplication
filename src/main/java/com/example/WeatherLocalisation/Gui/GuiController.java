package com.example.WeatherLocalisation.Gui;

import com.example.WeatherLocalisation.Gui.Gui;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.ArrayList;
import java.util.List;


@Controller
public class GuiController {



    private List<Gui> guiList;

    public GuiController() {//konstruktor do załadowania danych początkowych

        Gui gui0 = new Gui("x");
        guiList = new ArrayList<>();
        guiList.add(gui0);


    }

    @GetMapping("/gui")
    public String get(Model model) {
        //Gui gui = new Gui();

        model.addAttribute("guiList", guiList);
        model.addAttribute("newItems", new Gui());

        return "gui";
    }

    @PostMapping("/add-item")
    public String addItem(@ModelAttribute Gui gui) {
        System.out.println(gui);

        //guiList.add(gui);
        guiList.set(0, gui); //nadpisanie zerowego elementu
        return "redirect:/gui"; //przekazanie do metody wyżej bo w niej jest zdefinowane newCar
    }

    public Gui testExport() { //metoda do pobrania wartosci wpisanej przez uzytkownaika (trafia do MethodBase)
        Gui temp = guiList.get(0);


        return temp;
    }
    public boolean ifExist() {
        boolean val = guiList.contains(guiList.get(0));
        return val;
    }
}
