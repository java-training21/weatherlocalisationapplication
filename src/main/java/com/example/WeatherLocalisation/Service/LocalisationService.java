package com.example.WeatherLocalisation.Service;

import com.example.WeatherLocalisation.Entity.Localisation;
import com.example.WeatherLocalisation.Repository.LocalisationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocalisationService {
    @Autowired
    private LocalisationRepository localisationRepository;

    public List<Localisation> listAllLocalisation() {
        return (List<Localisation>) localisationRepository.findAll();
    }

    public void saveLocalisation(Localisation localisation) {
        localisationRepository.save(localisation);
    }

    public Localisation getLocalisation(Long id) {
        return localisationRepository.findById(id).get();
    }

    public void deleteLocalisation(Long id) {
        localisationRepository.deleteById(id);
    }
}
