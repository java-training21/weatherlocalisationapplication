package com.example.WeatherLocalisation;

import com.example.WeatherLocalisation.Repository.FindMinHistoricalTempRepository;
import com.example.WeatherLocalisation.jsonschema2pojo.jsonHistoricalWeather.HistoricalWeather;
import com.example.WeatherLocalisation.jsonschema2pojo.jsonLocalisation.City;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;
import java.util.TimeZone;

@Component
public class MethodBase {


    @Autowired// add for save to db possibility
    private FindMinHistoricalTempRepository findMinHistoricalTempRepository;
    public MethodBase(FindMinHistoricalTempRepository findMinHistoricalTempRepository) {
        this.findMinHistoricalTempRepository = findMinHistoricalTempRepository;
    }

    public MethodBase() {

    }


    public String ReadCityMethod(String APIkey) {

            String newJsonLocalisation;

//        try {

//            String APIkey = "ae540659c8c5d5c140a5e4cbc3ecccbf";

            Scanner scanVillage = new Scanner(System.in);
            System.out.println("City : ");
            String village = scanVillage.nextLine();

//        GuiController guiController = new GuiController();
//        String village = String.valueOf(guiController.testExport());
//        System.out.println(village);


            String urlLocalistion = "https://api.openweathermap.org/geo/1.0/direct?q=" + village + "&limit=1&" + "appid=" + APIkey;
            System.out.println("Url localisation: " + urlLocalistion);

            Client client = new Client();
            WebResource webResource = client.resource(urlLocalistion);
            ClientResponse webResponse = webResource.accept("application/json").get(ClientResponse.class);

            if (webResponse.getStatus() != 200) {
                throw new RuntimeException("HTTP Error ..." + webResponse.getStatus());
            }

            String jsonLocalisation = webResponse.getEntity(String.class);
            System.out.println("JSON: " + jsonLocalisation);

            int jsonLocalisationLength = jsonLocalisation.length();
            newJsonLocalisation = jsonLocalisation.substring(1, jsonLocalisationLength - 1);
            System.out.println("New json: " + newJsonLocalisation);// pozbycie się '[' i ']'


//        } catch (Exception e) {
//            e.printStackTrace();
//        }

            return newJsonLocalisation;

    }



    public int formProtMethod() {

        String userInput;
        int var0 = 1;
        int temp3 = 1;
        Scanner scannerResp = new Scanner(System.in);

        while(var0 == 1) {                                                              //format protection
            userInput = scannerResp.next(); //scan the input
            try {
                temp3 = Integer.parseInt(userInput); // try to parse the "number" to int

                if (temp3 != 0 && temp3 != 1) {
                    System.out.println("Wrong value. Please type 1 or 0.");
                }
                else {
                    var0 = 0;
                }
            } catch (NumberFormatException e) {
                System.out.println("Hey, you entered something wrong... You should type a number!");
            }
        }


        return temp3;

    }


    public void goodByePrinter() {
        System.out.println("");
        System.out.println("");
        System.out.println("   ______    ___     ___  ______     ______ ____  ____ ________ ");
        System.out.println(" .' ___  | .'   `. .'   `|_   _ `.  |_   _ |_  _||_  _|_   __  |");
        System.out.println("/ .'   \\_|/  .-.  /  .-.  \\| | `. \\   | |_) |\\ \\  / /   | |_ \\_| ");
        System.out.println("| |   ____| |   | | |   | || |  | |   |  __'. \\ \\/ /    |  _| _  ");
        System.out.println("\\ `.___]  \\  `-'  \\  `-'  _| |_.' /  _| |__) |_|  |_   _| |__/ |");
        System.out.println(" `._____.' `.___.' `.___.|______.'  |_______/|______| |________|");
        System.out.println("");
        System.out.println("");
    }


    public void logoPrinter() {
        System.out.println("");
        System.out.println(" __          __        _   _                          _____  _____  ");
        System.out.println(" \\ \\        / /       | | | |                   /\\   |  __ \\|  __ \\ ");
        System.out.println("  \\ \\  /\\  / ___  __ _| |_| |__   ___ _ __     /  \\  | |__) | |__) |");
        System.out.println("   \\ \\/  \\/ / _ \\/ _` | __| '_ \\ / _ | '__|   / /\\ \\ |  ___/|  ___/ ");
        System.out.println("    \\  /\\  |  __| (_| | |_| | | |  __| |     / ____ \\| |    | |     ");
        System.out.println("     \\/  \\/ \\___|\\__,_|\\__|_| |_|\\___|_|    /_/    \\_|_|    |_|     ");
        System.out.println("                                                                    ");
        System.out.println("");
    }



    public String currentWeather(String newJsonLocalisation, String APIkey) throws JsonProcessingException { //throws JsonPrecessingException zamaist tego można zrobic try catch

        ObjectMapper mapperLocalisation = new ObjectMapper();//to jest do przerobki bo city jest tylko do lat i lon
        City city = mapperLocalisation.readValue(newJsonLocalisation, City.class);


        String units = "metric";
        String lang = "PL";
        double lat = city.getLat() ;
        double lon = city.getLon();
        String urlWeather = "https://api.openweathermap.org/data/2.5/weather?" + "lat=" + lat + "&lon=" + lon + "&appid=" + APIkey + "&units=" + units + "&lang=" + lang;
        System.out.println("");
        System.out.println("ulrWeather: " + urlWeather);


        Client client2 = new Client();
        WebResource webResource2 = client2.resource(urlWeather);
        ClientResponse webResponse2 = webResource2.accept("application/json").get(ClientResponse.class);

        if (webResponse2.getStatus() != 200) {
            throw new RuntimeException("HTTP Error ..." + webResponse2.getStatus());
        }

        String jsonWeather = webResponse2.getEntity(String.class);
        System.out.println("JSON: " + jsonWeather);

        return jsonWeather;
    }



    public String getFromExternalJSONapi(String url) throws JsonProcessingException { //z powodu readValue lub można użyć try catch

                System.out.println("URL: " + url);
                Client client = new Client();
                WebResource webResource = client.resource(url);
                ClientResponse webResponse = webResource.accept("application/json").get(ClientResponse.class);

                if (webResponse.getStatus() != 200) {
                    throw new RuntimeException("HTTP Error ..." + webResponse.getStatus());
                }
                String json = webResponse.getEntity(String.class);
                System.out.println("JSON: " + json);

                return json;
    }



    public long timeConversion(String time) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm", Locale.ENGLISH); //Specify your locale
        long unixTime = 0;
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+1:00")); //Specify your timezone
        try {
            unixTime = dateFormat.parse(time).getTime();
            unixTime = unixTime / 1000;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return unixTime;
    }


    public String converterMethod(int unixtimestamp) {

        Date date = new Date(unixtimestamp*1000L);
        SimpleDateFormat jdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
        jdf.setTimeZone(TimeZone.getTimeZone("GMT+1"));
        String java_date = jdf.format(date);
        return java_date;
    }






    //@Autowired //with this and without this method will run before StartLogic 2x!!!
//    @EventListener(ApplicationReadyEvent.class) switch off
    public void findMinTemperature () throws JsonProcessingException { //exception because readValue

        //metoda działa w tan sposób, że pobiera dane w pętli (aktualnie jest zdefiniowana na 5 dni) i pobiera dla nich dane historyczne. Jest mozliwość zapisu tych pobranyc danych
        //do bazy. Atualnie funkcjonalnosc ta jest wykomentowana aby nie zapełniać niepotrzebnie bazy.

        String newJsonLocalisation;
//        try {
        String APIkey = "ae540659c8c5d5c140a5e4cbc3ecccbf";
        Scanner scanVillage = new Scanner(System.in);
        System.out.println("findMinTemperature");
        System.out.println();
        System.out.println("findMinTemp -> City: ");
        String village = scanVillage.nextLine();
        String urlLocalistion = "https://api.openweathermap.org/geo/1.0/direct?q=" + village + "&limit=1&" + "appid=" + APIkey;
        System.out.println("Url localisation: " + urlLocalistion);

        Client client = new Client();
        WebResource webResource = client.resource(urlLocalistion);
        ClientResponse webResponse = webResource.accept("application/json").get(ClientResponse.class);

        if (webResponse.getStatus() != 200) {
            throw new RuntimeException("HTTP Error ..." + webResponse.getStatus());
        }

        String jsonLocalisation = webResponse.getEntity(String.class);
        System.out.println("JSON: " + jsonLocalisation);

        int jsonLocalisationLength = jsonLocalisation.length();
        newJsonLocalisation = jsonLocalisation.substring(1, jsonLocalisationLength - 1);
        System.out.println("New json: " + newJsonLocalisation);// get rid of '[' i ']'


//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        //return newJsonLocalisation;


        //uzycie Jackson'a 2  https://en.wikipedia.org/wiki/Jackson_(API)
        ObjectMapper mapperLocalisation = new ObjectMapper();
        City city = mapperLocalisation.readValue(newJsonLocalisation, City.class);

        System.out.println("City name: " + city.getName());
        System.out.println("Longitue: " + city.getLon());
        System.out.println("Latitude: " + city.getLat());
        System.out.println("Country: " + city.getCountry());
        System.out.println("State: " + city.getState());

        if (city.getLocalNames() != null)
            System.out.println(city.getLocalNames().getPl());


        String units = "metric";
        String lang = "PL";
        double lat = city.getLat();
        double lon = city.getLon();



        //Here you need to define your period for historical weather data  (max 1000 per one day for free option of API OpenWeather)
        int count = 0;
        int start = 309240057; //from    1979-10-20 05:00    https://www.unixtimestamp.com/
        int back = 308808057; //to       1979-05-15 05:00
        //Summary it will get 5 pack of data fot definied 5 days.
        while (back < start) {
            back = back + 86400; //one day = 86400sec/60sec/60min = 24h
            count++;
            System.out.println("Count: " + count);
            System.out.println("back: " + back);

            String urlHistoricalWeather = "https://api.openweathermap.org/data/3.0/onecall/timemachine?" + "lat=" + lat + "&lon=" + lon + "&appid=" + APIkey + "&units=" + units + "&lang=" + lang + "&dt=" + back;

            Client client4 = new Client();
            WebResource webResource4 = client4.resource(urlHistoricalWeather);
            ClientResponse webResponse4 = webResource4.accept("application/json").get(ClientResponse.class);

            if (webResponse4.getStatus() != 200) {
                throw new RuntimeException("HTTP Error ..." + webResponse4.getStatus());
            }

            String jsonHistoricalWeather = webResponse4.getEntity(String.class);
            System.out.println("JSON: " + jsonHistoricalWeather);


            // Jackson 2  https://en.wikipedia.org/wiki/Jackson_(API)
            ObjectMapper mapperHistoricalWeather = new ObjectMapper();
            HistoricalWeather historicalWeather = mapperHistoricalWeather.readValue(jsonHistoricalWeather, HistoricalWeather.class);

            System.out.println("");
            System.out.println(" SAVED Historical weather parameters: ");

            int histDate = historicalWeather.getData().get(0).getDt();
            //MethodBase methodBase = new MethodBase();
            Date date = new Date(back * 1000L);
            SimpleDateFormat jdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
            jdf.setTimeZone(TimeZone.getTimeZone("GMT+1"));
            String java_date = jdf.format(date);


            //String histDateString = methodBase.converterMethod(histDate); //methodBase
            //System.out.println("Historical weather (UnixTimestamp): " + historicalWeather.getData().get(0).getDt());
            System.out.println("Historical weather: " + java_date);
            System.out.println("Historical Weather Temperature: " + historicalWeather.getData().get(0).getTemp());
            System.out.println("Historical Weather Feels Like: " + historicalWeather.getData().get(0).getFeelsLike());
            System.out.println("Historical Weather Pressure: " + historicalWeather.getData().get(0).getPressure());
            System.out.println("Historical Weather Humidity: " + historicalWeather.getData().get(0).getHumidity());
            System.out.println("Historical Weather Clouds: " + historicalWeather.getData().get(0).getClouds());
            System.out.println("Historical Weather Wind Speed: " + historicalWeather.getData().get(0).getWindSpeed());
            System.out.println("Historical Weather main: " + historicalWeather.getData().get(0).getWeather().get(0).getMain());
            System.out.println("Historical Weather description: " + historicalWeather.getData().get(0).getWeather().get(0).getDescription());


            String db_data = java_date;
            double db_temperature = historicalWeather.getData().get(0).getTemp();
            double db_feelsLike = historicalWeather.getData().get(0).getFeelsLike();
            double db_pressure = historicalWeather.getData().get(0).getPressure();
            double db_humidity = historicalWeather.getData().get(0).getHumidity();
            int db_clouds = historicalWeather.getData().get(0).getClouds();
            double db_wind = historicalWeather.getData().get(0).getWindSpeed();
            String db_main = historicalWeather.getData().get(0).getWeather().get(0).getMain();
            String db_desc = historicalWeather.getData().get(0).getWeather().get(0).getDescription();


//            FindMinHistoricalTemp findMinHistoricalTemp = new FindMinHistoricalTemp(temp, feelsLike, pressure, humidity, clouds, wind, main, desc);
////            findMinHistoricalTempRepository.save(findMinHistoricalTemp);
//            FindMinHistoricalTemp findMinHistoricalTemp = new FindMinHistoricalTemp(temperature);
//            findMinHistoricalTempRepository.save(findMinHistoricalTemp);


//            String[] tab = new String[8];
//            tab[0] = String.valueOf(historicalWeather.getData().get(0).getTemp());
//            tab[1] = String.valueOf(historicalWeather.getData().get(0).getFeelsLike());
//            tab[2] = String.valueOf(historicalWeather.getData().get(0).getPressure());
//            tab[3] = String.valueOf(historicalWeather.getData().get(0).getHumidity());
//            tab[4] = Integer.toString(historicalWeather.getData().get(0).getClouds());
//            tab[5] = String.valueOf(historicalWeather.getData().get(0).getWindSpeed());
//            //tab[6] = historicalWeather.getData().get(0).getWeather().get(0).getMain();
//            tab[6] = historicalWeather.getData().get(0).getWeather().get(0).getDescription();


//            Localisation db_localisation = new Localisation(tab[0], tab[1], tab[2]);
//            localisationRepository.save(db_localisation);

//            for(int i=0;i<7;i++){
//                System.out.print(" ["+i+"]" + tab[i]);
//            }

            //DB save:
            //FindMinHistoricalTemp findMinHistoricalTemp = new FindMinHistoricalTemp(db_data, db_temperature, db_feelsLike, db_pressure, db_humidity, db_clouds, db_wind, db_desc);
            //findMinHistoricalTempRepository.save(findMinHistoricalTemp);
            System.out.println("DB save function is OFF");

        }
        System.out.println("sum Count: " + count);
        System.out.println("sum back: " + back);


    }


    //@EventListener(ApplicationReadyEvent.class)
    public void testTest() {
        System.out.println("TEST TEST TEST");
    }


    //@EventListener(ApplicationReadyEvent.class)
    public void testTest2() {
        System.out.print("test");
    }




}
