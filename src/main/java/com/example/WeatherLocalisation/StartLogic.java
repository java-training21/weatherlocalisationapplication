package com.example.WeatherLocalisation;

//w tym pliku jest realizacja pobirania informacji z API OpenWeather

import com.example.WeatherLocalisation.Entity.Localisation;
import com.example.WeatherLocalisation.Repository.FindMinHistoricalTempRepository;
import com.example.WeatherLocalisation.Repository.LocalisationRepository;
import com.example.WeatherLocalisation.jsonschema2pojo.jsonForecast.ForecastWeather;
import com.example.WeatherLocalisation.jsonschema2pojo.jsonHistoricalWeather.HistoricalWeather;
import com.example.WeatherLocalisation.jsonschema2pojo.jsonLocalisation.City;
import com.example.WeatherLocalisation.jsonschema2pojo.jsonWeather.Weather;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class StartLogic {

    @Autowired// add for save to db possibility
    private LocalisationRepository localisationRepository;
    private FindMinHistoricalTempRepository findMinHistoricalTempRepository;
    public StartLogic(LocalisationRepository localisationRepository, FindMinHistoricalTempRepository findMinHistoricalTempRepository) {
        this.localisationRepository = localisationRepository;
        this.findMinHistoricalTempRepository = findMinHistoricalTempRepository;

    }
    public StartLogic() {

    }

    @Autowired
    MethodBase methodBase;

//    @Autowired// add for save to db possibility
//    private LocalisationRepository localisationRepository;
//    public StartLogic(LocalisationRepository localisationRepository) {
//        this.localisationRepository = localisationRepository;
//    }
//    @Autowired
//    private FindMinHistoricalTempRepository findMinHistoricalTempRepository;
//    public StartLogic(FindMinHistoricalTempRepository findMinHistoricalTempRepository) {
//        this.findMinHistoricalTempRepository = findMinHistoricalTempRepository;
//    }




    //@Autowired dodanie autowired tutaj spowoduje że ta metoda wykona się pierwsza (tzn przed metodą findMinTemperature w MethodBase)
    @EventListener(ApplicationReadyEvent.class) //uruchomienie w momencie startu aplikacji, jeśeli uruchamaimy projekt z WeatherLocalisationApplication to nalezy to zakomentować
    public void applicationMethod() throws JsonProcessingException { // throws JsonProcessingException exception dodane przez readValuue (jeśli nie ma try catch)

        //MethodBase methodBase = new MethodBase();

        methodBase.logoPrinter();

        int temp1 = 1;
        while(temp1 == 1) {
            try {

                //CITY Localisation Geocoding API https://openweathermap.org/api/geocoding-api

//                String APIkey = "ae540659c8c5d5c140a5e4cbc3ecccbf";
//                Scanner scanVillage = new Scanner(System.in);
//
//                System.out.println("Podaj miasto: ");
//
//
//                String village = scanVillage.nextLine();
//                String urlLocalistion = "https://api.openweathermap.org/geo/1.0/direct?q=" + village + "&limit=1&" + "appid=" + APIkey;
//                System.out.println("Url localisation: " + urlLocalistion);
//
//                Client client = new Client();
//                WebResource webResource = client.resource(urlLocalistion);
//                ClientResponse webResponse = webResource.accept("application/json").get(ClientResponse.class);
//
//                if (webResponse.getStatus() != 200) {
//                    throw new RuntimeException("HTTP Error ..." + webResponse.getStatus());
//                }
//
//                String jsonLocalisation = webResponse.getEntity(String.class);
//                System.out.println("JSON: " + jsonLocalisation);
//
//                int jsonLocalisationLength = jsonLocalisation.length();
//                String newJsonLocalisation = jsonLocalisation.substring(1, jsonLocalisationLength - 1);
//                System.out.println("Nowy json: " + newJsonLocalisation);// pozbycie się '[' i ']'

//                int var01 = 1;
//                while(var01 == 1) {
//                    System.out.println("Przejscie petli...");
//                    System.out.println(var01);
//                    GuiController guiController = new GuiController();
//                    Gui temp0 = guiController.testExport();
//                    String sTemp0 = String.valueOf(temp0);
//
//                    System.out.println("Czy jest x:" + sTemp0);
//                    boolean temp = guiController.ifExist();
//                    System.out.println(temp);
//
//                    if(sTemp0 != "x"){
//                        var01 = 0;
//                    }
//                    System.out.println(var01);
//
//                }

                String APIkey = "ae540659c8c5d5c140a5e4cbc3ecccbf";
                String newJsonLocalisation = methodBase.ReadCityMethod(APIkey);//Podanie miasta i na tej podstawie wygenerowanie JSONA


                //uzycie Jackson'a 2  https://en.wikipedia.org/wiki/Jackson_(API)
                ObjectMapper mapperLocalisation = new ObjectMapper();
                City city = mapperLocalisation.readValue(newJsonLocalisation, City.class);

                System.out.println("City name: " + city.getName());
                System.out.println("Longitue: " + city.getLon());
                System.out.println("Latitude: " + city.getLat());
                System.out.println("Country: " + city.getCountry());
                System.out.println("State: " + city.getState());

                if (city.getLocalNames() != null)
                    System.out.println(city.getLocalNames().getPl());




                //WEATHER Current weather data https://openweathermap.org/current
                String units = "metric";
                String lang = "PL";
                double lat = city.getLat() ;
                double lon = city.getLon();
//                String urlWeather = "https://api.openweathermap.org/data/2.5/weather?" + "lat=" + lat + "&lon=" + lon + "&appid=" + APIkey + "&units=" + units + "&lang=" + lang;
//                System.out.println("");
//                System.out.println("ulrWeather: " + urlWeather);
//
//
//                Client client2 = new Client();
//                WebResource webResource2 = client2.resource(urlWeather);
//                ClientResponse webResponse2 = webResource2.accept("application/json").get(ClientResponse.class);
//
//                if (webResponse2.getStatus() != 200) {
//                    throw new RuntimeException("HTTP Error ..." + webResponse2.getStatus());
//                }
//
//                String jsonWeather = webResponse2.getEntity(String.class);
//                System.out.println("JSON: " + jsonWeather);
                String jsonWeather = methodBase.currentWeather(newJsonLocalisation, APIkey); //zamiast tego co wyżej



                //uzycie Jackson'a 2  https://en.wikipedia.org/wiki/Jackson_(API)
                ObjectMapper mapperWeather2 = new ObjectMapper();
                Weather weather = mapperWeather2.readValue(jsonWeather, Weather.class);

                System.out.println("");
                System.out.println("Current weather parameters: ");

                System.out.println("Weather_dt: " + weather.getDt());
                String date01 = methodBase.converterMethod(weather.getDt()); //methodBase
                System.out.println("Weather date: " + date01);

                String date02 = methodBase.converterMethod(weather.getSys().getSunrise()); //methodBase
                System.out.println("Sunrise: " + date02);
                String date03 = methodBase.converterMethod(weather.getSys().getSunset()); //methodBase
                System.out.println("Sunset: " + date03);

                System.out.println("Weather_id " + weather.getWeatherSpec().get(0).getId());
                System.out.println("Weather_main: " + weather.getWeatherSpec().get(0).getMain());
                System.out.println("Weather_description: " + weather.getWeatherSpec().get(0).getDescription());
                System.out.println("Weather_icon: " + weather.getWeatherSpec().get(0).getIcon());

                System.out.println("Coord Lon: " + weather.getCoord().getLon());
                System.out.println("Coord Lat: " + weather.getCoord().getLat());

                System.out.println("Main temp: " + weather.getMain().getTemp() + " st.C");
                System.out.println("Main feels_like: " + weather.getMain().getFeelsLike() + " st.C");
                System.out.println("Main temp_min: " + weather.getMain().getTempMin() + " st.C");
                System.out.println("Main temp_max: " + weather.getMain().getTempMax() + " st.C");
                System.out.println("Main pressure: " + weather.getMain().getPressure() + " HPa");
                System.out.println("Main humidity: " + weather.getMain().getHumidity() + " %");

                System.out.println("Visibility: " + weather.getVisibility() + " m");

                System.out.println("Wind_speed: " + weather.getWind().getSpeed() + " m/s");
                System.out.println("Wind_deg: " + weather.getWind().getDeg() + " deg");

                System.out.println("Clouds: " + weather.getClouds().getAll() + " %");



                //Do zapisu do bazy, do tabeli Localisation: (select * from SpringMySQLAPI.localisation;)

                //Localisation
                double db_lon = city.getLon();
                double db_lat = city.getLat();
                String db_country = city.getCountry();
                String db_state = city.getState();
                String db_city =city.getName();
                //Weather
                int do_weather_dt = weather.getDt();
                String db_Weather_date = date01;
                String db_sunrise = date02;
                String db_weather_id = date03;
                String db_weather_main = weather.getWeatherSpec().get(0).getMain();
                String db_weather_description = weather.getWeatherSpec().get(0).getDescription();
                double db_weather_temp = weather.getMain().getTemp();
                double db_weather_feels_like = weather.getMain().getFeelsLike();

                Localisation db_localisation = new Localisation(db_lon, db_lat, db_country, db_state, db_city, db_Weather_date, db_sunrise, db_weather_id, db_weather_main, db_weather_description, db_weather_temp, db_weather_feels_like);
                localisationRepository.save(db_localisation);





                //WEATHER FORECAST 5 day weather forecast https://openweathermap.org/forecast5

//                System.out.println("Czy chcesz pobrac prognozę? [Yes: 1  No: 0]");
//                int temp3 = methodBase.formProtMethod();
//                while(temp3 == 1) {
//
//                    Scanner scannerCnt = new Scanner(System.in);
//                    System.out.println("Podaj za ile dni chcesz sprawdzić pogodę ([9= +24h], [17= +48h], [25= +72h], [33= +4dni], [40= +4dni21h] [max 40]): ");
//
//                    int cnt = scannerCnt.nextInt();
//                    String urlForecast = "https://api.openweathermap.org/data/2.5/forecast?" + "lat=" + lat + "&lon=" + lon + "&appid=" + APIkey + "&units=" + units + "&lang=" + lang + "&cnt=" + cnt;
//                    System.out.println("");
//                    System.out.println("ulrWeather: " + urlForecast);
//
//                    Client client3 = new Client();
//                    WebResource webResource3 = client3.resource(urlForecast);
//                    ClientResponse webResponse3 = webResource3.accept("application/json").get(ClientResponse.class);
//
//                    if (webResponse3.getStatus() != 200) {
//                        throw new RuntimeException("HTTP Error ..." + webResponse3.getStatus());
//                    }
//
//                    String jsonForecast = webResponse3.getEntity(String.class);
//                    System.out.println("JSON Forecast: " + jsonForecast);
//
//
//                    //uzycie Jackson'a 2  https://en.wikipedia.org/wiki/Jackson_(API)
//                    ObjectMapper mapperWeather3 = new ObjectMapper();
//                    ForecastWeather forcastweather = mapperWeather3.readValue(jsonForecast, ForecastWeather.class);
//
//                    System.out.println("");
//                    System.out.println("Forecast weather parameters: ");
//
//                    System.out.println("Forecast cnt (~3h): " + forcastweather.getCnt());
//
//                    int unix_seconds1 = forcastweather.getList().get(cnt - 1).getDt();
//                    String java_date1 = converterUnixToDate.converterMethod(unix_seconds1);
//                    System.out.println("unix seconds: " + unix_seconds1);
//                    System.out.println("################################### Forecast dt: " + java_date1 + " ###########");
//
//                    System.out.println("###################################Forecast temp: " + forcastweather.getList().get(cnt - 1).getMain().getTemp() + "st.C ############");
//                    System.out.println("################################### Forecast feels-like temp: " + forcastweather.getList().get(cnt - 1).getMain().getFeelsLike() + "st.C ###########");
//                    System.out.println("Humidity: " + forcastweather.getList().get(cnt - 1).getMain().getHumidity());
//                    System.out.println("Sea level: " + forcastweather.getList().get(cnt - 1).getMain().getSeaLevel());
//
//                    System.out.println("Czy chcesz pobrać prognozę na inny termin? [Yes:1 No:0]");
//                    temp3 = methodBase.formProtMethod();
//
//                }




                System.out.println("Do you want to get the weather forecast ? [Yes: 1  No: 0]"); //2 sposob z wykorzystaniem metody w MethodBase
                int temp4 = methodBase.formProtMethod();
                while(temp4 == 1) {
                    Scanner scannerCnt = new Scanner(System.in);
                    System.out.println("Which number of days/hours? ([9= +24h], [17= +48h], [25= +72h], [33= +4dni], [40= +4dni21h] [max 40]): ");
                    int cnt = scannerCnt.nextInt();
                    String urlForecast = "https://api.openweathermap.org/data/2.5/forecast?" + "lat=" + lat + "&lon=" + lon + "&appid=" + APIkey + "&units=" + units + "&lang=" + lang + "&cnt=" + cnt;

                    String jsonForecast = methodBase.getFromExternalJSONapi(urlForecast);

                    //uzycie Jackson'a 2  https://en.wikipedia.org/wiki/Jackson_(API)
                    ObjectMapper mapperWeather3 = new ObjectMapper();
                    ForecastWeather forcastweather = mapperWeather3.readValue(jsonForecast, ForecastWeather.class);

                    System.out.println("");
                    System.out.println("Forecast weather parameters: ");

                    System.out.println("Forecast cnt (~3h): " + forcastweather.getCnt());

                    int unix_seconds1 = forcastweather.getList().get(cnt - 1).getDt();
                    String java_date1 = methodBase.converterMethod(unix_seconds1); //methodBase
                    System.out.println("unix seconds: " + unix_seconds1);
                    System.out.println("################################### Forecast dt: " + java_date1 + " ###########");

                    System.out.println("###################################Forecast temp: " + forcastweather.getList().get(cnt - 1).getMain().getTemp() + "st.C ############");
                    System.out.println("################################### Forecast feels-like temp: " + forcastweather.getList().get(cnt - 1).getMain().getFeelsLike() + "st.C ###########");
                    System.out.println("Humidity: " + forcastweather.getList().get(cnt - 1).getMain().getHumidity());
                    System.out.println("Sea level: " + forcastweather.getList().get(cnt - 1).getMain().getSeaLevel());

                    System.out.println("Do you want to get the weather forecast for the another date? [Yes:1 No:0]");
                    temp4 = methodBase.formProtMethod();

                }






                //HISTORICAL WEATHER API: https://openweathermap.org/api/one-call-3
                //Historical weather data

                System.out.println("Do you want to get the historical data? [Yes:1 No:0]: ");
                int temp2 = methodBase.formProtMethod();

                while(temp2 == 1) {

                    System.out.println("Put the date: [YYYY-MM-DD-HH-MM]: ");
                    Scanner scannerHistDate = new Scanner(System.in);
                    String dataHistoricalUnixTimestamp = scannerHistDate.nextLine();

                    System.out.println("Date in unixTimestamp (new method): " + methodBase.timeConversion(dataHistoricalUnixTimestamp));
                    Long dataHistoricalAfterConv = methodBase.timeConversion(dataHistoricalUnixTimestamp);

                    String urlHistoricalWeather = "https://api.openweathermap.org/data/3.0/onecall/timemachine?" + "lat=" + lat + "&lon=" + lon + "&appid=" + APIkey + "&units=" + units + "&lang=" + lang + "&dt=" + dataHistoricalAfterConv;
                    //System.out.println("");
                    //System.out.println("ulrHistoricalWeather: " + urlHistoricalWeather);

                    String jsonHistoricalWeather = methodBase.getFromExternalJSONapi(urlHistoricalWeather);

//                    Client client4 = new Client();
//                    WebResource webResource4 = client4.resource(urlHistoricalWeather);
//                    ClientResponse webResponse4 = webResource4.accept("application/json").get(ClientResponse.class);
//
//                    if (webResponse4.getStatus() != 200) {
//                        throw new RuntimeException("HTTP Error ..." + webResponse4.getStatus());
//                    }
//
//                    String jsonHistoricalWeather = webResponse4.getEntity(String.class);
//                    System.out.println("JSON: " + jsonHistoricalWeather);


                    //uzycie Jackson'a 2  https://en.wikipedia.org/wiki/Jackson_(API)
                    ObjectMapper mapperHistoricalWeather = new ObjectMapper();
                    HistoricalWeather historicalWeather = mapperHistoricalWeather.readValue(jsonHistoricalWeather, HistoricalWeather.class);


                    System.out.println("");
                    System.out.println("Historical weather parameters: ");

                    int histDate = historicalWeather.getData().get(0).getDt();
                    String histDateString = methodBase.converterMethod(histDate); //methodBase
                    System.out.println("Historical weather (UnixTimestamp): " + historicalWeather.getData().get(0).getDt());
                    System.out.println("Historical weather: " + histDateString);

                    int histSunrise = historicalWeather.getData().get(0).getSunrise();
                    String histSunriseString = methodBase.converterMethod(histSunrise); //methodBase
                    System.out.println("Historical weather sunrise (UnixTimestamp): " + historicalWeather.getData().get(0).getSunrise());
                    System.out.println("Historical weather sunrise: " + histSunriseString);

                    int histSunset = historicalWeather.getData().get(0).getSunset();
                    String histSunsetString = methodBase.converterMethod(histSunset); //methodBase
                    System.out.println("Historical weather sunset (InixTimestamp): " + historicalWeather.getData().get(0).getSunset());
                    System.out.println("Historical weather sunset: " + histSunsetString);

                    System.out.println("Historical Weather Temperature: " + historicalWeather.getData().get(0).getTemp());
                    System.out.println("Historical Weather Feels Like: " + historicalWeather.getData().get(0).getFeelsLike());
                    System.out.println("Historical Weather Pressure: " + historicalWeather.getData().get(0).getPressure());
                    System.out.println("Historical Weather Humidity: " + historicalWeather.getData().get(0).getHumidity());
                    System.out.println("Historical Weather Clouds: " + historicalWeather.getData().get(0).getClouds());
                    System.out.println("Historical Weather Wind Speed: " + historicalWeather.getData().get(0).getWindSpeed());
                    System.out.println("Historical Weather main: " + historicalWeather.getData().get(0).getWeather().get(0).getMain());
                    System.out.println("Historical Weather description: " + historicalWeather.getData().get(0).getWeather().get(0).getDescription());


                    if(historicalWeather.getData().get(0).getSnow() != null){
                        System.out.println("Historical Weather snow (/h): " + historicalWeather.getData().get(0).getSnow().get1h());
                    }
                    else System.out.println("There is no snow data for this localisation and date!");

                    System.out.println();
                    System.out.println("Do you want to choose another date? [Yes:1 No:0]: ");
                    temp2 = methodBase.formProtMethod();

                }



                //findMinHistoricaltemp
                int var1 = 1;
                System.out.println("Do you want to get the pack of historical weather data? (need to be configured in findMinTemperature method) [Yes=1, No=0]");
                var1 = methodBase.formProtMethod();
                while(var1 == 1) {

                    methodBase.findMinTemperature(); //wywołując tę metodę tutaj nstępuje błąd: java.lang.NullPointerException dla zapis do bazy !!! Naprawione poprzez dodanie @Autowired MethodBase methodBase;

                    System.out.println("Do you want to get again? [Yes=1, No=0]");
                    var1=methodBase.formProtMethod();
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

            System.out.println("");
            System.out.println("Do you want to choose another localisation? [YES:1 NO:0]");
            temp1 = methodBase.formProtMethod();


        }
        methodBase.goodByePrinter();


    }
}
