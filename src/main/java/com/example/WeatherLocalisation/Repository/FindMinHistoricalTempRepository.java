package com.example.WeatherLocalisation.Repository;

import com.example.WeatherLocalisation.Entity.FindMinHistoricalTemp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FindMinHistoricalTempRepository extends JpaRepository<FindMinHistoricalTemp, Long> {
}