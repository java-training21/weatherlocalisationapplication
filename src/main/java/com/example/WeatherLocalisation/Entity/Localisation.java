package com.example.WeatherLocalisation.Entity;

import lombok.Getter;
import lombok.Setter;

//this class is for own API creation with selected variables which values are downloaded from external Weather API
//endpoint according to the LocalisationController: http://localhost:8080/api/localisation
//it is connected with the React frontend application: fullstrackweatherapi

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
@Getter
@Setter
public class Localisation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Localisation(double lon, double lat, String country, String state, String city, String weather_date, String db_sunrise, String db_weather_id, String db_weather_main, String db_weather_desc, double db_weather_temp, double db_weather_feels_like) {
    //public Localisation(String temperature, String feelsLike, String pressure ) {
        this.lon = lon;
        this.lat = lat;
        this.country = country;
        this.state = state;
        this.city = city;
        //-----------------------------------added for my own API:
        this.db_weather_date = weather_date;
        this.db_sunrise = db_sunrise;
        this.db_weather_id = db_weather_id;
        this.db_weather_main = db_weather_main;
        this.db_weather_desc = db_weather_desc;
        this.db_weather_temp = db_weather_temp;
        this.db_weather_feels_like = db_weather_feels_like;
        //dodane testowo:
//        this.temperature = temperature;
//        this.feelsLike = feelsLike;
//        this.pressure = pressure;
    }

    public Localisation() {

    }

    private double lon;
    private double lat;
    private String country;
    private String state;
    private String city;
    private String db_weather_date;
    private String db_sunrise;
    private String db_weather_id;
    private String db_weather_main;
    private String db_weather_desc;
    private double db_weather_temp;
    private double db_weather_feels_like;



    //dodane testowo:
//    private String temperature;
//    private String feelsLike;
//    private String pressure;



    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    public String getWeather_date() {
        return db_weather_date;
    }
    public void setWeather_date(String weather_date) {
        this.db_weather_date = weather_date;
    }

    public String getDb_weather_date() {
        return db_weather_date;
    }

    public void setDb_weather_date(String db_weather_date) {
        this.db_weather_date = db_weather_date;
    }

    public String getDb_sunrise() {
        return db_sunrise;
    }

    public void setDb_sunrise(String db_sunrise) {
        this.db_sunrise = db_sunrise;
    }

    public String getDb_weather_id() {
        return db_weather_id;
    }

    public void setDb_weather_id(String db_weather_id) {
        this.db_weather_id = db_weather_id;
    }

    public String getDb_weather_main() {
        return db_weather_main;
    }

    public void setDb_weather_main(String db_weather_main) {
        this.db_weather_main = db_weather_main;
    }

    public String getDb_weather_description() {
        return db_weather_desc;
    }

    public void setDb_weather_description(String db_weather_description) {
        this.db_weather_desc = db_weather_description;
    }

    public String getDb_weather_desc() {
        return db_weather_desc;
    }

    public void setDb_weather_desc(String db_weather_desc) {
        this.db_weather_desc = db_weather_desc;
    }

    public double getDb_weather_temp() {
        return db_weather_temp;
    }

    public void setDb_weather_temp(double db_weather_temp) {
        this.db_weather_temp = db_weather_temp;
    }

    public double getDb_weather_feels_like() {
        return db_weather_feels_like;
    }

    public void setDb_weather_feels_like(double db_weather_feels_like) {
        this.db_weather_feels_like = db_weather_feels_like;
    }
}
