package com.example.WeatherLocalisation.jsonschema2pojo.jsonLocalisation;

//https://www.jsonschema2pojo.org/

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "lt",
        "it",
        "mk",
        "cs",
        "la",
        "he",
        "zh",
        "sk",
        "en",
        "bg",
        "eo",
        "yi",
        "lv",
        "de",
        "fr",
        "be",
        "hu",
        "pt",
        "ja",
        "uk",
        "sr",
        "es",
        "ru",
        "pl",
        "ar"
})
@Generated("jsonschema2pojo")
public class LocalNames {

    @JsonProperty("lt")
    private String lt;
    @JsonProperty("it")
    private String it;
    @JsonProperty("mk")
    private String mk;
    @JsonProperty("cs")
    private String cs;
    @JsonProperty("la")
    private String la;
    @JsonProperty("he")
    private String he;
    @JsonProperty("zh")
    private String zh;
    @JsonProperty("sk")
    private String sk;
    @JsonProperty("en")
    private String en;
    @JsonProperty("bg")
    private String bg;
    @JsonProperty("eo")
    private String eo;
    @JsonProperty("yi")
    private String yi;
    @JsonProperty("lv")
    private String lv;
    @JsonProperty("de")
    private String de;
    @JsonProperty("fr")
    private String fr;
    @JsonProperty("be")
    private String be;
    @JsonProperty("hu")
    private String hu;
    @JsonProperty("pt")
    private String pt;
    @JsonProperty("ja")
    private String ja;
    @JsonProperty("uk")
    private String uk;
    @JsonProperty("sr")
    private String sr;
    @JsonProperty("es")
    private String es;
    @JsonProperty("ru")
    private String ru;
    @JsonProperty("pl")
    private String pl;
    @JsonProperty("ar")
    private String ar;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("lt")
    public String getLt() {
        return lt;
    }

    @JsonProperty("lt")
    public void setLt(String lt) {
        this.lt = lt;
    }

    @JsonProperty("it")
    public String getIt() {
        return it;
    }

    @JsonProperty("it")
    public void setIt(String it) {
        this.it = it;
    }

    @JsonProperty("mk")
    public String getMk() {
        return mk;
    }

    @JsonProperty("mk")
    public void setMk(String mk) {
        this.mk = mk;
    }

    @JsonProperty("cs")
    public String getCs() {
        return cs;
    }

    @JsonProperty("cs")
    public void setCs(String cs) {
        this.cs = cs;
    }

    @JsonProperty("la")
    public String getLa() {
        return la;
    }

    @JsonProperty("la")
    public void setLa(String la) {
        this.la = la;
    }

    @JsonProperty("he")
    public String getHe() {
        return he;
    }

    @JsonProperty("he")
    public void setHe(String he) {
        this.he = he;
    }

    @JsonProperty("zh")
    public String getZh() {
        return zh;
    }

    @JsonProperty("zh")
    public void setZh(String zh) {
        this.zh = zh;
    }

    @JsonProperty("sk")
    public String getSk() {
        return sk;
    }

    @JsonProperty("sk")
    public void setSk(String sk) {
        this.sk = sk;
    }

    @JsonProperty("en")
    public String getEn() {
        return en;
    }

    @JsonProperty("en")
    public void setEn(String en) {
        this.en = en;
    }

    @JsonProperty("bg")
    public String getBg() {
        return bg;
    }

    @JsonProperty("bg")
    public void setBg(String bg) {
        this.bg = bg;
    }

    @JsonProperty("eo")
    public String getEo() {
        return eo;
    }

    @JsonProperty("eo")
    public void setEo(String eo) {
        this.eo = eo;
    }

    @JsonProperty("yi")
    public String getYi() {
        return yi;
    }

    @JsonProperty("yi")
    public void setYi(String yi) {
        this.yi = yi;
    }

    @JsonProperty("lv")
    public String getLv() {
        return lv;
    }

    @JsonProperty("lv")
    public void setLv(String lv) {
        this.lv = lv;
    }

    @JsonProperty("de")
    public String getDe() {
        return de;
    }

    @JsonProperty("de")
    public void setDe(String de) {
        this.de = de;
    }

    @JsonProperty("fr")
    public String getFr() {
        return fr;
    }

    @JsonProperty("fr")
    public void setFr(String fr) {
        this.fr = fr;
    }

    @JsonProperty("be")
    public String getBe() {
        return be;
    }

    @JsonProperty("be")
    public void setBe(String be) {
        this.be = be;
    }

    @JsonProperty("hu")
    public String getHu() {
        return hu;
    }

    @JsonProperty("hu")
    public void setHu(String hu) {
        this.hu = hu;
    }

    @JsonProperty("pt")
    public String getPt() {
        return pt;
    }

    @JsonProperty("pt")
    public void setPt(String pt) {
        this.pt = pt;
    }

    @JsonProperty("ja")
    public String getJa() {
        return ja;
    }

    @JsonProperty("ja")
    public void setJa(String ja) {
        this.ja = ja;
    }

    @JsonProperty("uk")
    public String getUk() {
        return uk;
    }

    @JsonProperty("uk")
    public void setUk(String uk) {
        this.uk = uk;
    }

    @JsonProperty("sr")
    public String getSr() {
        return sr;
    }

    @JsonProperty("sr")
    public void setSr(String sr) {
        this.sr = sr;
    }

    @JsonProperty("es")
    public String getEs() {
        return es;
    }

    @JsonProperty("es")
    public void setEs(String es) {
        this.es = es;
    }

    @JsonProperty("ru")
    public String getRu() {
        return ru;
    }

    @JsonProperty("ru")
    public void setRu(String ru) {
        this.ru = ru;
    }

    @JsonProperty("pl")
    public String getPl() {
        return pl;
    }

    @JsonProperty("pl")
    public void setPl(String pl) {
        this.pl = pl;
    }

    @JsonProperty("ar")
    public String getAr() {
        return ar;
    }

    @JsonProperty("ar")
    public void setAr(String ar) {
        this.ar = ar;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
