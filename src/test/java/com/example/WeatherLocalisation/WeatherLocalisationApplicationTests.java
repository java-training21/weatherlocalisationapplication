package com.example.WeatherLocalisation;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.NoSuchElementException;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

//@SpringBootTest //For testing should be commented (switched off) EvenListener command in StartLogic.java class (line 54)
@RunWith(JUnit4.class)//it allows not to comment (switch off) the EventListener! :)
class WeatherLocalisationApplicationTests {
	private MethodBase methodBase;

	@BeforeEach
    public void beforeEach() {
	    methodBase = new MethodBase();
    }

	@Test
	public void getFromExternalJSONapiTest() throws JsonProcessingException {
	    //given
		String expectedResult = "{\"cod\":\"200\",\"message\":0,\"cnt\":20,\"list\":[{\"dt\":1674324000,\"main\":{\"temp\":27.78,\"feels_like\":30.75,\"temp_min\":27.77,\"temp_max\":27.78,\"pressure\":1009,\"sea_level\":1009,\"grnd_level\":1010,\"humidity\":74,\"temp_kf\":0.01},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"zachmurzenie duże\",\"icon\":\"04d\"}],\"clouds\":{\"all\":90},\"wind\":{\"speed\":5.04,\"deg\":201,\"gust\":4.9},\"visibility\":10000,\"pop\":0.16,\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2023-01-21 18:00:00\"},{\"dt\":1674334800,\"main\":{\"temp\":27.74,\"feels_like\":30.79,\"temp_min\":27.71,\"temp_max\":27.74,\"pressure\":1011,\"sea_level\":1011,\"grnd_level\":1012,\"humidity\":75,\"temp_kf\":0.03},\"weather\":[{\"id\":500,\"main\":\"Rain\",\"description\":\"słabe opady deszczu\",\"icon\":\"10n\"}],\"clouds\":{\"all\":76},\"wind\":{\"speed\":4.19,\"deg\":221,\"gust\":4.01},\"visibility\":10000,\"pop\":0.36,\"rain\":{\"3h\":0.5},\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2023-01-21 21:00:00\"},{\"dt\":1674345600,\"main\":{\"temp\":27.41,\"feels_like\":30.33,\"temp_min\":27.41,\"temp_max\":27.41,\"pressure\":1011,\"sea_level\":1011,\"grnd_level\":1011,\"humidity\":77,\"temp_kf\":0},\"weather\":[{\"id\":500,\"main\":\"Rain\",\"description\":\"słabe opady deszczu\",\"icon\":\"10n\"}],\"clouds\":{\"all\":82},\"wind\":{\"speed\":3.44,\"deg\":228,\"gust\":3.4},\"visibility\":10000,\"pop\":0.68,\"rain\":{\"3h\":0.75},\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2023-01-22 00:00:00\"},{\"dt\":1674356400,\"main\":{\"temp\":27.16,\"feels_like\":29.89,\"temp_min\":27.16,\"temp_max\":27.16,\"pressure\":1010,\"sea_level\":1010,\"grnd_level\":1010,\"humidity\":78,\"temp_kf\":0},\"weather\":[{\"id\":500,\"main\":\"Rain\",\"description\":\"słabe opady deszczu\",\"icon\":\"10n\"}],\"clouds\":{\"all\":100},\"wind\":{\"speed\":3.98,\"deg\":243,\"gust\":4.1},\"visibility\":10000,\"pop\":0.88,\"rain\":{\"3h\":1.31},\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2023-01-22 03:00:00\"},{\"dt\":1674367200,\"main\":{\"temp\":27.39,\"feels_like\":30.17,\"temp_min\":27.39,\"temp_max\":27.39,\"pressure\":1010,\"sea_level\":1010,\"grnd_level\":1010,\"humidity\":76,\"temp_kf\":0},\"weather\":[{\"id\":500,\"main\":\"Rain\",\"description\":\"słabe opady deszczu\",\"icon\":\"10n\"}],\"clouds\":{\"all\":100},\"wind\":{\"speed\":4.07,\"deg\":216,\"gust\":4},\"visibility\":10000,\"pop\":0.96,\"rain\":{\"3h\":1.56},\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2023-01-22 06:00:00\"},{\"dt\":1674378000,\"main\":{\"temp\":27.7,\"feels_like\":30.83,\"temp_min\":27.7,\"temp_max\":27.7,\"pressure\":1011,\"sea_level\":1011,\"grnd_level\":1011,\"humidity\":76,\"temp_kf\":0},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"zachmurzenie duże\",\"icon\":\"04d\"}],\"clouds\":{\"all\":100},\"wind\":{\"speed\":5.88,\"deg\":210,\"gust\":5.81},\"visibility\":10000,\"pop\":0.2,\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2023-01-22 09:00:00\"},{\"dt\":1674388800,\"main\":{\"temp\":27.72,\"feels_like\":30.62,\"temp_min\":27.72,\"temp_max\":27.72,\"pressure\":1010,\"sea_level\":1010,\"grnd_level\":1010,\"humidity\":74,\"temp_kf\":0},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"zachmurzenie duże\",\"icon\":\"04d\"}],\"clouds\":{\"all\":100},\"wind\":{\"speed\":5.85,\"deg\":213,\"gust\":5.8},\"visibility\":10000,\"pop\":0.08,\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2023-01-22 12:00:00\"},{\"dt\":1674399600,\"main\":{\"temp\":27.82,\"feels_like\":30.7,\"temp_min\":27.82,\"temp_max\":27.82,\"pressure\":1009,\"sea_level\":1009,\"grnd_level\":1009,\"humidity\":73,\"temp_kf\":0},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"zachmurzenie duże\",\"icon\":\"04d\"}],\"clouds\":{\"all\":100},\"wind\":{\"speed\":4.7,\"deg\":216,\"gust\":4.6},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2023-01-22 15:00:00\"},{\"dt\":1674410400,\"main\":{\"temp\":27.86,\"feels_like\":30.78,\"temp_min\":27.86,\"temp_max\":27.86,\"pressure\":1010,\"sea_level\":1010,\"grnd_level\":1010,\"humidity\":73,\"temp_kf\":0},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"zachmurzenie duże\",\"icon\":\"04d\"}],\"clouds\":{\"all\":100},\"wind\":{\"speed\":3.76,\"deg\":224,\"gust\":3.61},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2023-01-22 18:00:00\"},{\"dt\":1674421200,\"main\":{\"temp\":27.78,\"feels_like\":30.62,\"temp_min\":27.78,\"temp_max\":27.78,\"pressure\":1011,\"sea_level\":1011,\"grnd_level\":1011,\"humidity\":73,\"temp_kf\":0},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"zachmurzenie duże\",\"icon\":\"04n\"}],\"clouds\":{\"all\":99},\"wind\":{\"speed\":3.26,\"deg\":234,\"gust\":3.11},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2023-01-22 21:00:00\"},{\"dt\":1674432000,\"main\":{\"temp\":27.6,\"feels_like\":30.38,\"temp_min\":27.6,\"temp_max\":27.6,\"pressure\":1011,\"sea_level\":1011,\"grnd_level\":1011,\"humidity\":74,\"temp_kf\":0},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"zachmurzenie duże\",\"icon\":\"04n\"}],\"clouds\":{\"all\":95},\"wind\":{\"speed\":3.82,\"deg\":234,\"gust\":3.72},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2023-01-23 00:00:00\"},{\"dt\":1674442800,\"main\":{\"temp\":27.4,\"feels_like\":30.19,\"temp_min\":27.4,\"temp_max\":27.4,\"pressure\":1009,\"sea_level\":1009,\"grnd_level\":1009,\"humidity\":76,\"temp_kf\":0},\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"zachmurzenie umiarkowane\",\"icon\":\"04n\"}],\"clouds\":{\"all\":75},\"wind\":{\"speed\":4,\"deg\":210,\"gust\":3.92},\"visibility\":10000,\"pop\":0.04,\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2023-01-23 03:00:00\"},{\"dt\":1674453600,\"main\":{\"temp\":27.47,\"feels_like\":30.22,\"temp_min\":27.47,\"temp_max\":27.47,\"pressure\":1010,\"sea_level\":1010,\"grnd_level\":1010,\"humidity\":75,\"temp_kf\":0},\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"zachmurzenie umiarkowane\",\"icon\":\"04n\"}],\"clouds\":{\"all\":53},\"wind\":{\"speed\":3.79,\"deg\":222,\"gust\":3.71},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2023-01-23 06:00:00\"},{\"dt\":1674464400,\"main\":{\"temp\":27.66,\"feels_like\":30.75,\"temp_min\":27.66,\"temp_max\":27.66,\"pressure\":1012,\"sea_level\":1012,\"grnd_level\":1012,\"humidity\":76,\"temp_kf\":0},\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"zachmurzenie umiarkowane\",\"icon\":\"04d\"}],\"clouds\":{\"all\":66},\"wind\":{\"speed\":5.23,\"deg\":214,\"gust\":5.2},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2023-01-23 09:00:00\"},{\"dt\":1674475200,\"main\":{\"temp\":27.75,\"feels_like\":30.81,\"temp_min\":27.75,\"temp_max\":27.75,\"pressure\":1011,\"sea_level\":1011,\"grnd_level\":1011,\"humidity\":75,\"temp_kf\":0},\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"zachmurzenie umiarkowane\",\"icon\":\"04d\"}],\"clouds\":{\"all\":64},\"wind\":{\"speed\":4.42,\"deg\":229,\"gust\":4.3},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2023-01-23 12:00:00\"},{\"dt\":1674486000,\"main\":{\"temp\":27.85,\"feels_like\":31.03,\"temp_min\":27.85,\"temp_max\":27.85,\"pressure\":1008,\"sea_level\":1008,\"grnd_level\":1008,\"humidity\":75,\"temp_kf\":0},\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"zachmurzenie umiarkowane\",\"icon\":\"04d\"}],\"clouds\":{\"all\":73},\"wind\":{\"speed\":4.23,\"deg\":228,\"gust\":4.1},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2023-01-23 15:00:00\"},{\"dt\":1674496800,\"main\":{\"temp\":27.99,\"feels_like\":31.33,\"temp_min\":27.99,\"temp_max\":27.99,\"pressure\":1009,\"sea_level\":1009,\"grnd_level\":1009,\"humidity\":75,\"temp_kf\":0},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"zachmurzenie duże\",\"icon\":\"04d\"}],\"clouds\":{\"all\":86},\"wind\":{\"speed\":3.68,\"deg\":224,\"gust\":3.52},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2023-01-23 18:00:00\"},{\"dt\":1674507600,\"main\":{\"temp\":27.95,\"feels_like\":31.11,\"temp_min\":27.95,\"temp_max\":27.95,\"pressure\":1012,\"sea_level\":1012,\"grnd_level\":1012,\"humidity\":74,\"temp_kf\":0},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"zachmurzenie duże\",\"icon\":\"04n\"}],\"clouds\":{\"all\":100},\"wind\":{\"speed\":3.76,\"deg\":206,\"gust\":3.61},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2023-01-23 21:00:00\"},{\"dt\":1674518400,\"main\":{\"temp\":27.73,\"feels_like\":30.77,\"temp_min\":27.73,\"temp_max\":27.73,\"pressure\":1011,\"sea_level\":1011,\"grnd_level\":1011,\"humidity\":75,\"temp_kf\":0},\"weather\":[{\"id\":500,\"main\":\"Rain\",\"description\":\"słabe opady deszczu\",\"icon\":\"10n\"}],\"clouds\":{\"all\":91},\"wind\":{\"speed\":3.45,\"deg\":213,\"gust\":3.4},\"visibility\":10000,\"pop\":0.36,\"rain\":{\"3h\":0.13},\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2023-01-24 00:00:00\"},{\"dt\":1674529200,\"main\":{\"temp\":27.61,\"feels_like\":30.64,\"temp_min\":27.61,\"temp_max\":27.61,\"pressure\":1009,\"sea_level\":1009,\"grnd_level\":1009,\"humidity\":76,\"temp_kf\":0},\"weather\":[{\"id\":500,\"main\":\"Rain\",\"description\":\"słabe opady deszczu\",\"icon\":\"10n\"}],\"clouds\":{\"all\":85},\"wind\":{\"speed\":4.09,\"deg\":218,\"gust\":4},\"visibility\":10000,\"pop\":0.48,\"rain\":{\"3h\":0.19},\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2023-01-24 03:00:00\"}],\"city\":{\"id\":0,\"name\":\"\",\"coord\":{\"lat\":1,\"lon\":1},\"country\":\"\",\"population\":0,\"timezone\":0,\"sunrise\":1674281096,\"sunset\":1674324545}}";
		int lon = 1;
		int lat = 1;
		String APIkey = "ae540659c8c5d5c140a5e4cbc3ecccbf";
		String units = "metric";
		String lang = "PL";
		String cnt = "20";
		String urlForecast = "https://api.openweathermap.org/data/2.5/forecast?" + "lat=" + lat + "&lon=" + lon + "&appid=" + APIkey + "&units=" + units + "&lang=" + lang + "&cnt=" + cnt;

		//when
		String actualresult = methodBase.getFromExternalJSONapi(urlForecast);

        //then
		//Assertions.assertEquals(expectedResult, actualresult);
		org.assertj.core.api.Assertions.assertThat(actualresult).containsAnyOf("coordxD", "lon"); //it is looking for minimum one equal value
		org.assertj.core.api.Assertions.assertThat(actualresult).contains("cod","message","cnt","list","dt","main","temp","feels_like","temp_min","temp_max","pressure","sea_level","humidity","clouds");
		//all of above have to be fulfilled
    }
	@Test
	public void currentWeatherTest() throws JsonProcessingException {
		//given
		String newJsonLocalisation = "{\"name\":\"Kartuzy\",\"local_names\":{\"ru\":\"Картузы\",\"uk\":\"Картузи\",\"pl\":\"Kartuzy\",\"de\":\"Karthaus\",\"lt\":\"Kartūzai\"},\"lat\":54.3342281,\"lon\":18.1973639,\"country\":\"PL\",\"state\":\"Pomeranian Voivodeship\"}";
		String APIkey = "ae540659c8c5d5c140a5e4cbc3ecccbf";
		String expectedValue = "{\"coord\":{\"lon\":18.1974,\"lat\":54.3342},\"weather\":[{\"id\":602,\"main\":\"Snow\",\"description\":\"silne opady śniegu\",\"icon\":\"13n\"}],\"base\":\"stations\",\"main\":{\"temp\":0.57,\"feels_like\":-5.86,\"temp_min\":-1.33,\"temp_max\":2.44,\"pressure\":1025,\"humidity\":98},\"visibility\":600,\"wind\":{\"speed\":8.75,\"deg\":40},\"snow\":{\"1h\":1.46},\"clouds\":{\"all\":100},\"dt\":1674317788,\"sys\":{\"type\":2,\"id\":2030689,\"country\":\"PL\",\"sunrise\":1674283979,\"sunset\":1674313405},\"timezone\":3600,\"id\":3096525,\"name\":\"Kartuzy\",\"cod\":200}";

		//when
		String actualvalue = methodBase.currentWeather(newJsonLocalisation, APIkey);

		//then
		//Assertions.assertEquals(expectedValue, actualvalue);
		org.assertj.core.api.Assertions.assertThat(actualvalue).containsAnyOf("coordxD", "lon"); //szuka przynajmniej jeden z wzorców
		org.assertj.core.api.Assertions.assertThat(actualvalue).contains("{\"coord\":{\"lon\":","lon","lat","weather","id","main","description","icon","base","temp","feels_like","temp_min","temp_max","pressure","humidity","visibility","wind","speed","deg","clouds","id","name");
	}
	@Test
	public void timeConversionTest() {
		//given
		String time = "2022-01-01-12-00-00";
		//when
		Long unixTime = methodBase.timeConversion(time);
		//then
		org.assertj.core.api.Assertions.assertThat(unixTime).isEqualTo(1641034800);
			//https://www.unixtimestamp.com/
	}
	@Test
	public void converterMethodTest() {
		//given
		int unixTimestamp = 1641034800;
		//when
		String java_date = methodBase.converterMethod(unixTimestamp);
		//then
		org.assertj.core.api.Assertions.assertThat(java_date).isEqualTo("2022-01-01 12:00:00 GMT+01:00");
	}



	//private final InputStream systemIn = System.in;
	//private final PrintStream systemOut = System.out;
	@Test
	public void ReadCityMethodTest_happyPath() {
		//given
		String apiKey = "ae540659c8c5d5c140a5e4cbc3ecccbf";
		String input = "Kartuzy";
		InputStream in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);
		//test ustawia strumień wejściowy na predefiniowany ciąg znaków i z niego korzysta scanner przy wywołaniu testowanej metody

		//String expectedResult = "{\"name\":\"Kartuzy\",\"local_names\":{\"ru\":\"Картузы\",\"uk\":\"Картузи\",\"pl\":\"Kartuzy\",\"de\":\"Karthaus\",\"lt\":\"Kartūzai\"},\"lat\":54.3342281,\"lon\":18.1973639,\"country\":\"PL\",\"state\":\"Pomeranian Voivodeship\"}";
		String expectedOutput_urlLocalisation = "https://api.openweathermap.org/geo/1.0/direct?q=Kartuzy&limit=1&appid=ae540659c8c5d5c140a5e4cbc3ecccbf";

		//when
		String result = methodBase.ReadCityMethod(apiKey);

		//then
		//org.assertj.core.api.Assertions.assertThat(result).isEqualTo(expectedResult);
		org.assertj.core.api.Assertions.assertThat(result).contains("name","Kartuzy","local_names","ru","Картузы","uk","Картузи","de","Karthaus","lt","Kartūzai","lat","54.3342281","lon","18.1973639","country","PL","state","Pomeranian Voivodeship");

	}
	@Test
	public void ReadCityMethodTest_negative() {
		//given
		String apiKey = "ae540659c8c5d5c140a5e4cbc3ecccbf"+"error123";
		String input = "Kartuzy";
		InputStream in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);
		//test ustawia strumień wejściowy na predefiniowany ciąg znaków i z niego korzysta scanner przy wywołaniu testowanej metody

		//String expectedResult = "{\"name\":\"Kartuzy\",\"local_names\":{\"ru\":\"Картузы\",\"uk\":\"Картузи\",\"pl\":\"Kartuzy\",\"de\":\"Karthaus\",\"lt\":\"Kartūzai\"},\"lat\":54.3342281,\"lon\":18.1973639,\"country\":\"PL\",\"state\":\"Pomeranian Voivodeship\"}";
		String expectedOutput_urlLocalisation = "https://api.openweathermap.org/geo/1.0/direct?q=Kartuzy&limit=1&appid=ae540659c8c5d5c140a5e4cbc3ecccbf";


		// Sprawdzenie, czy metoda rzuca wyjątek NoSuchElementException
//		assertThrows(RuntimeException.class, () -> {
//			methodBase.ReadCityMethod(apiKey);
//		});
		// Sprawdzenie, czy wyjątek ma odpowiednią treść
		Exception exception = assertThrows(RuntimeException.class, () -> {
			methodBase.ReadCityMethod(apiKey);
		});

		//then
		String expectedMessage = "HTTP Error ...";
		String actualMessage = exception.getMessage();
		org.assertj.core.api.Assertions.assertThat(actualMessage).contains(expectedMessage);

	}

	@Test
	public void formProtMethod_test_positive() {

		for(int i = 0;i<=1;i++){
			//given
			String input = String.valueOf(i);
			InputStream in = new ByteArrayInputStream(input.getBytes());
			System.setIn(in);

			int expectedValue = i;

			//when
			int result = methodBase.formProtMethod();

			//then
			org.assertj.core.api.Assertions.assertThat(result).isEqualTo(expectedValue);
		}
	}

	@Test
	public void formProtMethod_test_negative() {
		//given
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(); //method returns error message using System.out.println()...
		System.setOut(new PrintStream(outputStream));

		String errorMessage = "Hey, you entered something wrong... You should type a number!";
		String input = "r";
		InputStream in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);

		//when
		// Sprawdzenie, czy metoda rzuca wyjątek NoSuchElementException
		assertThrows(NoSuchElementException.class, () -> {
			methodBase.formProtMethod();
		});

		// Sprawdzenie, czy wyjątek ma odpowiednią treść (w ten sposób się nie da ponieważ wyjątek zwraca swój opis poprzez System.out.println...)
//		Exception exception = assertThrows(NoSuchElementException.class, () -> {
//			methodBase.formProtMethod();
//		});

		//then
		String consoleOutput = outputStream.toString().trim();
		org.assertj.core.api.Assertions.assertThat(errorMessage).isEqualTo(consoleOutput);

		assertEquals(errorMessage, consoleOutput);
		System.setOut(System.out); //Ustawienie standardowego wyjścia za pomocą System.setOut(System.out), aby nie wpłynęło to na inne testy

	}
	@Test
	public void formProtMethod_test_negative2() {
		//given
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(); //method returns error message using System.out.println()...
		System.setOut(new PrintStream(outputStream));

		String errorMessage = "Wrong value. Please type 1 or 0.";
		String input = "2";
		InputStream in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);

		//when
		// Sprawdzenie, czy metoda rzuca wyjątek NoSuchElementException
		assertThrows(NoSuchElementException.class, () -> {
			methodBase.formProtMethod();
		});

		// Sprawdzenie, czy wyjątek ma odpowiednią treść (w ten sposób się nie da ponieważ wyjątek zwraca swój opis poprzez System.out.println...)
//		Exception exception = assertThrows(NoSuchElementException.class, () -> {
//			methodBase.formProtMethod();
//		});

		//then
		String consoleOutput = outputStream.toString().trim();
		org.assertj.core.api.Assertions.assertThat(errorMessage).isEqualTo(consoleOutput);

		assertEquals(errorMessage, consoleOutput);
		System.setOut(System.out); //Ustawienie standardowego wyjścia za pomocą System.setOut(System.out), aby nie wpłynęło to na inne testy

	}
	@Test
	public void testTest2Test() {

		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));


		methodBase.testTest2();

		Assertions.assertEquals("test", outContent.toString());

	}



}

