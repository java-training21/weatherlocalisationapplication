FROM adoptopenjdk/openjdk8:ubi
ADD target/WeatherLocalisation-0.0.1-SNAPSHOT.jar .
EXPOSE 8000
CMD java -jar WeatherLocalisation-0.0.1-SNAPSHOT.jar